# ids.js表意文字渲染工具
本工具可将表意文字描述符“⿰⿱⿲⿳⿴⿵⿶⿷⿸⿹⿺⿻”渲染为真实的汉字（说简单点，就是用来在网页上显示自己造的汉字的）  
## 使用方法
先加载ids.js和ids.css两个文件，然后用decodeIDS(str)将str解析为汉字  
也可以使用下面的代码，将[ids][/ids]标签中的内容解析为汉字  

```
    str = str.replace(/\[ids\]([^\[]*)\[\/ids\]/g, function(res){
      try{
        var str = res.replace("[ids]","");
        str = str.replace("[/ids]","");
        var ch = decodeIDS(str);
        return "<div style='display: inline-block;position:relative;'>"+ch+"</div>";
      }catch(e){
        console.log(e);
      }
      return "无法解析";
    });

```
## 效果
![img](test.png)  
[实际效果(翻到页尾)](https://sinriv.com/psg1)  
修改自[ids_renderer](https://github.com/hedalu244/ids_renderer)
